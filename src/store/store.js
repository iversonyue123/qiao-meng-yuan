
import login from './login/login.js'



// Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        login
    }
})