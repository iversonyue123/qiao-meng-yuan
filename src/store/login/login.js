import { stat } from "fs";

//定义状态
const state = {
    token: null,
    userName: null,
    pwd: null,
    code: null
}


const getters = {
    getToken(state) {
        if (!state.token) {
            state.token = localStorage.getItem('token')
        }
       return state.token
    },
    getUserName(state){
        if(!state.userName){
            state.userName = localStorage.getItem('userName')
        }
        return state.userName
    },
    getUserPwd(state){
        if(!state.pwd){
            state.pwd = localStorage.getItem('pwd')
        }
        return state.pwd
    },
    getCode(state) {
        if (!state.code) {
            state.code = localStorage.getItem('code')
        }
        return state.code
    }
}

const mutations = {
    SETTOKEN(state, token) {
        if(token){
            localStorage.setItem('token', token)
            state.token = token
        }
    },
    REMOVETOKEN(state, token){
        if(token){
            localStorage.removeItem(token)
        }
    },
    SETUSERNAME(state, userName){
        if(userName){
            localStorage.setItem('userName', userName)
            state.userName = userName
        }
    },
    SETUSERPWD(state, pwd){
        if(pwd){
            localStorage.setItem('pwd', pwd)
            state.pwd = pwd
        }
    },
    CLEARREMOVELOGIN(state){
        localStorage.removeItem('pwd')
    },
    SETCODE(state, code) {
        if (code) {
            localStorage.setItem('code', code)
            state.code = code
        }
    }

}

const actions = {
    setToken({commit}, token) {
        commit('SETTOKEN', token)
    },
    removeToken({commit}, token) {
        commit('REMOVETOKEN', token)
    },
    setUserName({commit}, userName) {
        commit('SETUSERNAME', userName)
    },
    setUserPwd({commit}, pwd) {
        commit('SETUSERPWD', pwd)
    },
    clearRemoveLogin({commit}) {
        commit('CLEARREMOVELOGIN')
    },
    setCode({commit}, code) {
        commit('SETCODE', code)
    }
}



export default {
    state,
    mutations,
    actions,
    getters
}