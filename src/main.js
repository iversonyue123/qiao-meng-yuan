// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Vue from 'vue'
import App from './App'
import router from './router/router'
import util from './network/common/util'
import store from './store/store.js'

//导入iView样式
Vue.config.productionTip = false
    // 轮播图
import VueAwesomeSwiper from 'vue-awesome-swiper'
require('swiper/dist/css/swiper.css')
Vue.use(VueAwesomeSwiper)
    //样式初始化
import '@/common/css/reset.css'
// import '@/common/less/theme.less'
Vue.prototype.axios = axios
    /* eslint-disable no-new */
router.beforeEach((to, from, next) => {
    iview.LoadingBar.start();
    const token = store.getters.getToken
    if (to.meta.auth == undefined || to.meta.auth == false) {
        next()
    } else {
        if (!token) {
            router.push({ name: 'login' })
        }
    }
    next();
});
router.afterEach(() => {
    iview.LoadingBar.finish();
    window.scrollTo(0, 0);
});

new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})