import util from '@/network/common/util.js';
import store from '@/store/store.js'

let api = {
    'getAdList': '/app/tb_ad/v1/getAdList', // 广告列表
    'getCompanyList': '/app/tb_company/v1/getCompanyList', // 获取企业列表
    'getHouseList': '/app/tb_house/v1/getHouseList', // 获取房源列表
};


// 广告列表
function getAdList(params) {
    let p = new Promise(function(resolove, reject) {
        util.ajax.post(api.getAdList, {
            startTime: params.startTime,
            endTime: params.endTime,
            page: params.page,
            pagesize: params.pagesize
        }).then(function(res) {
            resolove(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

// 企业列表
function getCompanyList(params, page, pagesize) {
    let p = new Promise(function(resolove, reject) {
        util.ajax.post(api.getCompanyList, {
                startTime: params.startTime,
                endTime: params.endTime,
                page: page,
                pagesize: pagesize
            }).then(function(res) {
                resolove(res)
            }),
            function(error) {
                reject(error)
            }
    })
    return p;
}

// 获取房源列表
function getHouseList(params, page, pagesize) {
    let p = new Promise(function(resolove, reject) {
        util.ajax.post(api.getHouseList, {
                tower: params.tower,
                startArea: params.startArea,
                endArea: params.endArea,
                page: page,
                pagesize: pagesize
            }).then(function(res) {
                resolove(res);
            }),
            function(error) {
                reject(error)
            }
    })
    return p;
}
export default {
    getAdList,
    getCompanyList,
    getHouseList
}