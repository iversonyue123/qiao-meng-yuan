export const register = {
    path: '/register',
    name: 'register',
    meta: {
        title: '注册',
    },
    // component: () => import('@/pages/register/Register')
}

export const login = {
    path: '/login',
    name: 'login',
    meta: {
        title: '登录',
    },
    component: () =>
        import ('@/pages/login/Login')
}
export const pasBack = {
    path: '/pasBack',
    name: 'pasBack',
    meta: {
        title: '忘记密码',
    },
    // component: () => import('@/pages/forget/PasBack')
}


export const app = {
    path: '/',
    redirect: {
        path: '/main/home'
    }
}

export const mainRouter = {
    path: '/main',
    component: () =>
        import ('@/pages/Main'),
    children: [{
        path: 'home',
        name: 'home',
        meta: {
            title: '首页',
        },
        component: () =>
            import ('@/pages/home/Index')
    }]
}






export default new VueRouter({
    mode: 'hash',
    routes: [
        register,
        login,
        pasBack,
        app,
        mainRouter,
    ]
})